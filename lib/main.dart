import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Daftar Negara Pendiri Asean'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(16),
                color: Colors.black38,
                child: Row(
                  children: [
                    Image.asset(
                      'images/indonesia.jpg',
                      width: 150,
                    ),
                    Container(
                      width: 170,
                      margin: EdgeInsets.all(25),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Text(
                            "Indonesia",
                            style: TextStyle(
                              fontSize: 25,
                            ),
                          ),
                          Text(
                            "jakarta",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),

              // NEGARA LAOS
              Container(
                margin: EdgeInsets.all(16),
                color: Colors.black38,
                child: Row(
                  children: [
                    Image.asset(
                      'images/laos.jpg',
                      width: 150,
                    ),
                    Container(
                      width: 170,
                      margin: EdgeInsets.all(25),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Text(
                            "Laos",
                            style: TextStyle(
                              fontSize: 25,
                            ),
                          ),
                          Text(
                            "Vientiane",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
// End LAOS

// NEGARA MALAYSIA
              Container(
                margin: EdgeInsets.all(16),
                color: Colors.black38,
                child: Row(
                  children: [
                    Image.asset(
                      'images/malaysia.png',
                      width: 150,
                    ),
                    Container(
                      width: 170,
                      margin: EdgeInsets.all(25),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Text(
                            "Malaysia",
                            style: TextStyle(
                              fontSize: 25,
                            ),
                          ),
                          Text(
                            "Kuala Lumpur",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
// End MALAYSIA

// NEGARA PHILIPINA
              Container(
                margin: EdgeInsets.all(16),
                color: Colors.black38,
                child: Row(
                  children: [
                    Image.asset(
                      'images/philipina.png',
                      width: 150,
                    ),
                    Container(
                      width: 170,
                      margin: EdgeInsets.all(25),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Text(
                            "Philipina",
                            style: TextStyle(
                              fontSize: 25,
                            ),
                          ),
                          Text(
                            "Manila",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
// End PHILIPINA
            ],
          ),
        )

        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
